from network.deeplabv3_3d import DeepLabV3_3D
from data.dataset import Dataset
from pathlib import Path
from utils.utils import arguments
import torch
from utils.predict import predict
from metrics import dice, generalized_dice
import numpy as np
from collections import OrderedDict
from skimage import io
import cv2
import sys, os

dirname = str(Path.cwd())

# Those paths need to be set
dataset_path = Path(dirname + ('/CarS_invivo'))
save_path = dirname + ('/Trained_Models/Pictures')
load_path = Path(dirname + ('/Trained_Models/_ce_nnunet_combined_999_0.0001/model_best_ce_nnunet_combined.pth'))


palette = OrderedDict(
        {
            "scaphoid": [0, 0, 128],
            "background": [0, 0, 0]
        })


args = arguments(num_classes=2, input_channels=1, batchsize=5, dataset_path=dataset_path, shuffle=False,
                          mode="test", num_epochs=300, learning_rate=0.001, last_activation='softmax', vol_depth=81,
                          palette=palette, pretrained=False, augmentation=False, weight_decay=True)

model = DeepLabV3_3D(args, resnet='resnet34_os16')
model.load_state_dict(torch.load(load_path))

test_dataset = Dataset(args=args)
test_generator = test_dataset.batch_generator()
dice_fn = generalized_dice()

batch = next(test_generator)
output, gt, images = predict(model, batch)
out = np.rint(output.detach().numpy())
gts = gt.detach().numpy()
print(dice(test=out, reference=gts))

print(dice_fn(torch.tensor(out), torch.tensor(gts)))
# store images to hard drive

for i in range(output.shape[0]):
    for j in range(output.shape[-1]):
        im = np.squeeze((images[i, 0, :, :, j]+1)/2.*255)
        im = np.stack((im, im, im), axis=2)
        groundtruth = torch.tensor(gt).permute(0, 2, 3, 1, 4)
        output_permuted = output.permute(0, 2, 3, 1, 4)
        groundtruth = np.squeeze(groundtruth[i, :, :, :, j])
        pred = np.squeeze(output_permuted[i, :, :, :, j])
        groundtruth = groundtruth.detach().numpy()
        pred = pred.detach().numpy()
        groundtruth = test_dataset.color_segmentation_from_logits(groundtruth)
        pred = test_dataset.color_segmentation_from_logits(pred)
        groundtruth_overlay = cv2.addWeighted(im,  1, groundtruth, 0.5, 0, dtype=cv2.CV_64F)
        pred_overlay = cv2.addWeighted(im,  1, pred, 0.5, 0, dtype=cv2.CV_64F)
        groundtruth = np.clip(groundtruth, a_min=0, a_max=255)
        pred = np.clip(pred, a_min=0, a_max=255)
        groundtruth_overlay = np.clip(groundtruth_overlay, a_min=0, a_max=255)
        pred_overlay = np.clip(pred_overlay, a_min=0, a_max=255)
        io.imsave(save_path + '/im/' + str((i+1)) + '/' + str(j).zfill(4) + "_im.png", im.astype(np.uint8), check_contrast=False)
        io.imsave(save_path + '/pred/' + str((i+1)) + '/' + str(j).zfill(4) + "_pred.png", pred.astype(np.uint8), check_contrast=False)
        io.imsave(save_path + '/gt/' + str((i+1)) + '/' + str(j).zfill(4) + "_gt.png", groundtruth.astype(np.uint8), check_contrast=False)
        io.imsave(save_path + '/gt_overlay/' + str((i+1)) + '/' + str(j).zfill(4) + "_gt.png", groundtruth_overlay.astype(np.uint8), check_contrast=False)
        io.imsave(save_path + '/pred_overlay/' + str((i+1)) + '/' + str(j).zfill(4) + "_pred.png", pred_overlay.astype(np.uint8), check_contrast=False)

