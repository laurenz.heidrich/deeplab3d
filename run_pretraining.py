from train import run_training
from pathlib import Path
from utils.utils import arguments
from collections import OrderedDict

output_path = Path(r'/hpcwork/im685561/Trying_Deeplab/Task04_Hippocampus/converted')

palette = OrderedDict(
        {
            "foreground": [255, 255, 255],
            "background": [0, 0, 0]
        })

dataset_path = Path(r'/hpcwork/im685561/Trying_DeeplabTask04_Hippocampus/converted')

args = arguments(num_classes=2, input_channels=1, batchsize=40, dataset_path=dataset_path, shuffle=True,
                     mode="pretrain", num_epochs=300, learning_rate=0.0001, last_activation='softmax', vol_depth=40,
                     palette=palette, weight_decay=True)

args_val = arguments(num_classes=2, input_channels=1, batchsize=5, dataset_path=dataset_path, shuffle=False,
                          mode="validate", num_epochs=300, learning_rate=0.0001, last_activation='softmax', vol_depth=40,
                          palette=palette, weight_decay=True)

run_training(arg=args, arg_val=args_val, output_path=output_path, loss_id="ce_nnunet_combined")




