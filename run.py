from train import run_training
from pathlib import Path
from utils.utils import arguments
from collections import OrderedDict

dirname = str(Path.cwd())
dataset_path = Path(dirname + ('/CarS_invivo'))
output_path = dirname + ('/Trained_Models')

palette = OrderedDict(
        {
            "scaphoid": [0, 0, 128],
            "background": [0, 0, 0]
        })

# Here, all arguments for the training need to be provided
args = arguments(num_classes=2, input_channels=1, batchsize=20, dataset_path=dataset_path, shuffle=True,
                     mode="train", num_epochs=1000, learning_rate=0.0001, last_activation='softmax', vol_depth=81,
                     palette=palette, pretrained=True, augmentation=True, weight_decay=True)

# Arguments for the validation dataset. Some arguments don't have any influence: num_epochs, learning_rate,
# last_activation, pretrained, augmentation, weight_decay. Those are only interesting above
args_val = arguments(num_classes=2, input_channels=1, batchsize=5, dataset_path=dataset_path, shuffle=False,
                          mode="validate", num_epochs=500, learning_rate=0.001, last_activation='softmax', vol_depth=81,
                          palette=palette, pretrained=False, augmentation=False, weight_decay=True)

run_training(arg=args, arg_val=args_val, output_path=output_path, loss_id="ce_nnunet_combined", resnet="resnet18_os8")


