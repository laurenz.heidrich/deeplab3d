def add_weight_decay(net, l2_value, skip_list=()):
    # https://raberrytv.wordpress.com/2017/10/29/pytorch-weight-decay-made-easy/

    decay, no_decay = [], []
    for name, param in net.named_parameters():
        if not param.requires_grad:
            continue  # frozen weights
        if len(param.shape) == 1 or name.endswith(".bias") or name in skip_list:
            no_decay.append(param)
        else:
            decay.append(param)

    return [{'params': no_decay, 'weight_decay': 0.0}, {'params': decay, 'weight_decay': l2_value}]


class arguments:
    def __init__(self, num_classes, input_channels, batchsize, dataset_path, shuffle, mode, num_epochs, learning_rate,
                 vol_depth, palette, weight_decay, pretrained, augmentation, last_activation=None):
        self.num_classes = num_classes            # Number of classes. (= number of output channel)
        self.input_channels = input_channels          # Number of input channel
        self.batchsize = batchsize
        self.dataset_path = dataset_path
        self.shuffle = shuffle
        self.mode = mode
        self.num_epochs = num_epochs
        self.learning_rate = learning_rate
        self.vol_depth = vol_depth
        self.palette = palette
        self.last_activation = last_activation
        self.weight_decay = weight_decay
        self.pretrained = pretrained
        self.augmentation = augmentation


def write_specs(text_path, args):

    f = open(text_path, "a")
    f.write(str(vars(args)))
    f.close()


def write_text(text_path, text: str):
    f = open(text_path, "a")
    f.write((text + "\n"))
    f.close()


