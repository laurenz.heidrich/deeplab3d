import torch

def predict(model, batch):
    model = model.float()
    model.eval()
    imgs = torch.tensor(batch[0]).float()
    gt = torch.tensor(batch[1]).float()
    return model(imgs), gt, imgs
