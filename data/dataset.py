import numpy as np
import cv2
import os
from random import shuffle
import random
import itertools
from pathlib import Path
from collections import OrderedDict
from typing import Generator, Tuple
from PIL import Image


class Dataset:

    def __init__(self, args):
        self.batchsize_train = args.batchsize
        self.dataset_path = args.dataset_path
        self.num_classes = args.num_classes
        self.shuffle = args.shuffle
        self.input_channels = args.input_channels
        self.mode = args.mode
        self.vol_depth = args.vol_depth
        self.palette = args.palette
        self.folders = []
        self.augmentation = args.augmentation
        self.flip = 0.5
        self.brightness_range = 0.2
        self.shear_range = 0.5
        self.width_range = 0.2
        self.height_range = 0.8

    def logits_from_color_segmentation(self, gt_color):
        gt_logits = np.zeros((gt_color.shape[0], gt_color.shape[1], self.num_classes))

        for i, c in enumerate(self.palette.items()):
            if i < self.num_classes - 1:
                m = np.all(gt_color == np.array(c[1]).reshape(1, 1, 3), axis=2)
                gt_logits[m, i] = 1

        m = np.all(gt_logits == np.zeros((1, 1, self.num_classes)), axis=2)
        gt_logits[m, -1] = 1
        return gt_logits

    def color_segmentation_from_logits(self, pred_logits):
        pred_color = np.zeros((pred_logits.shape[0], pred_logits.shape[1], 3))

        for i, c in enumerate(self.palette.items()):
            c = np.array(c[1]).reshape(1, 3)
            c[:, [0, 2]] = c[:, [2, 0]]
            if i < self.num_classes - 1:
                temp = np.argmax(pred_logits[:, :, :], axis=-1)
                I, J = np.nonzero(temp == i)
                for ind in range(len(I)):
                    pred_color[I[ind], J[ind], :] = c

        return pred_color

    def generator(self, partition: str, vol_depth):

        p = self.dataset_path
        im_path = p / partition / "im"
        gt_path = p / partition / "gt"
        im_ls = os.listdir(im_path)
        im_ls.sort()

        im_stacked = np.zeros((self.input_channels, 384, 384, vol_depth))
        gt_stacked = np.zeros((self.num_classes, 384, 384, vol_depth))

        if self.shuffle:
            shuffle(im_ls)

        for folder in itertools.cycle(im_ls):
            #print(folder)
            file_path = im_path / folder
            gt_file_path = gt_path / folder
            file_ls = os.listdir(file_path)
            file_ls.sort()
            self.folders.append(folder)
            j = 0

            for file in file_ls:
                if j < vol_depth:
                    stem = str(Path(file).stem)
                    im_p = file_path / file
                    #print(str(im_p))
                    gt_p = gt_file_path / (stem + ".png")

                    if im_p.exists():
                        im = cv2.resize(cv2.imread(str(im_p), cv2.IMREAD_GRAYSCALE), (384, 384))
                        im_stacked[0, :, :, j] = im
                    else:
                        im_stacked[0, :, :, j] = np.zeros((384, 384))

                    if gt_p.exists():
                        gt = cv2.resize(cv2.imread(str(gt_p), cv2.IMREAD_COLOR), (384, 384))
                        gt_stacked[0, :, :, j] = self.logits_from_color_segmentation(gt)[:, :, 0]
                        gt_stacked[1, :, :, j] = self.logits_from_color_segmentation(gt)[:, :, 1]

                    else:
                        gt_stacked[0, :, :, j] = np.zeros((384, 384))
                        gt_stacked[1, :, :, j] = np.ones((384, 384))

                    j = j + 1

            im_stacked = (im_stacked.astype(np.float32) / 127.5) - 1
            yield im_stacked.astype(np.float32), gt_stacked.astype(np.float32)

    def data_augmentation(self, base_generator):

        shear_range = self.shear_range * np.pi / 180

        for im, gt in base_generator:
            # Flip left-right
            if random.random() < self.flip:
                im = np.flip(im, axis=2)
                gt = np.flip(gt, axis=2)

            # Flip up-down
            if random.random() < self.flip:
                im = np.flip(im, axis=1)
                gt = np.flip(gt, axis=1)

            # Flip depth-wise
            if random.random() < self.flip:
                im = np.flip(im, axis=3)
                gt = np.flip(gt, axis=3)

            # Brightness Augmentation
            if self.brightness_range is not None:
                brightness_factor = random.uniform(1 - self.brightness_range, 1 + self.brightness_range)
                im = np.minimum(np.maximum(im * brightness_factor, -np.ones(im.shape)), np.ones(im.shape))

            # Rotation

            top_x1 = im.shape[2] * random.uniform(0, self.width_range / 2)
            top_x2 = im.shape[2] * (1 - random.uniform(0, self.width_range / 2))
            bot_y = im.shape[1] * random.uniform(self.height_range, 1)
            bot_x1 = np.amax([0., top_x1 + bot_y * np.tan(random.uniform(-self.shear_range / 2, self.shear_range / 2))])
            bot_x2 = np.amin([im.shape[2] - 1., top_x2 + bot_y * np.tan(random.uniform(-self.shear_range / 2, self.shear_range / 2))])

            # Set quad points
            quad_data = [top_x1, 0,
                         bot_x1, bot_y,
                         bot_x2, bot_y,
                         top_x2, 0]

            # Apply transform for images

            for depth in range(im.shape[3]):
                im[0, :, :, depth] = np.asarray(Image.fromarray(im[0, :, :, depth]).transform((im.shape[2], im.shape[1]), Image.QUAD,data=quad_data, resample=Image.BILINEAR))
            # Apply transform for groundtruth

            for dimension in range(0, self.num_classes):
                for depth in range(gt.shape[3]):
                    gt[dimension, :, :, depth] = np.asarray(Image.fromarray(gt[dimension, :, :, depth]).transform((im.shape[2], im.shape[1]), Image.QUAD, data=quad_data, resample=Image.BILINEAR))


            yield im, gt

    def provide(self, base_generator):
        for im, gt in base_generator:
            yield im, gt

    def batcherize(self, g, batchsize=5) -> Generator[Tuple[np.ndarray, np.ndarray], None, None]:
        """
        Converts a generator that yields single training example
        into a generator that yields batches of examples.
        :param g:
        :param batchsize:
        :return: Generator that yields samples of the form (...)
        """
        ims, gts = [], []
        for im, gt in g:
            if len(ims) == batchsize:
                yield np.array(ims), np.array(gts)
                ims, gts = [], []
            ims.append(im)
            gts.append(gt)

    def batch_generator(self):

        generator = self.generator(partition=self.mode, vol_depth=self.vol_depth)


        if(self.augmentation and self.mode == "train"):
            generator = self.data_augmentation(generator)

        g_batch = self.batcherize(self.provide(generator), batchsize=self.batchsize_train)

        return g_batch
