This is an 3D Deeplabv3+ implementation for semantic segementation of volumetric images. 
The original Deeplabv3 implementation can be found here: https://github.com/fregu856/deeplabv3
The 3D network architecture has been copied from: https://github.com/ChoiDM/pytorch-deeplabv3plus-3D
The architecture features most from the deeplabv3+ paper, except the decoder part. Right now, the encoder results are just naively upsampled.
The decoder needs to be implemented and special care needs to be taken to account for the two differen output strides (8 and 16)

The training, validation and test data has to be present in the following format:
    - Directory with three subdirectories called "test", "train", "validate"
    
    - Each subdirectory has to have two more subdirectories called "gt" and "im"

    - Each of those directories has to have directories that each stores one volumetric image. According image and gt folders need to be named 
        the same. The actual name can be chosen arbitrarily

    - The single images/gts that make up the volume should be stores as .png (other formats might work too), and should be named in a manner 
        that alphanumeric sorting results in the correct order of images

Training can be started by running run.py

The following arguments need to be provided inside run.py  

    - num_classes (= 2 for binary segmentation)

    - input_channels (= 1 for greyvalue images)

    - batchsize

    - dataset_path

    - shuffle

    - num_epochs

    - learning_rate (= 0.0001 or 0.001, depending)

    - last_activation (= "softmax", haven't tried anything else)

    - vol_depth (=81 for meditec scaphiod dataset)

    - palette (RGB values of background and foreground in gts)

    - pretrained (if pretrained models exist or trainig of a stopped model should continue. Path of pretrained model must be set in train.py)

    - augmentation (switches augmentation on/off, in data/dataset.py, the different augmentation can be switched on/off individually. Implemented
        are: mirroring in x,y,z direction, random crop, brightness augmentation)

    - weight_decay (=True)

    - loss_id (= "ce_nnunet_combined". Several loss function are implemented in losses.py. "ce_nnunet_combined", which is 
        combined_loss_robust_ce() in losses.py works best so far and is a combination of a dice loss and the CE loss used in nnunet, 
        which basically is just pytorch's CE loss with a compatibility layer)

    - resnet (All implemented resnets can be seen in network/deeplabv3_3d.py. For small datasets, resnet18_os8 has achieved the best results. If
        compute power or training time is limited resnet18_os16 is the next best choice. It performs a bit worse but is ~50% faster)


So far I have always trained on CPU. I have tried to train on GPU (minor code changes have to be done before), but GPU memory always ran out. Training was usually done on RWTH Compute Cluster. I have chosen 12 CPUs and 100GB RAM for resnet18_os16 and 150GB RAM for resnet18_os8. Epoch times were at ~160-170s for resnet18_os16 and ~230-240s for resnet18_os8. I have not done any testing wether 12 CPUs actually make sense, or if the amount of RAM is actually chosen efficiently.

The trained model can be tested by running test.py. A dice score will be calculated. This will also save predictions and overlays to disc if the respective lines of code are not commented out. Two dice score will be calculated, the first one is the nnunet-dice implementation and the second one is the meditec implmentation.

Not implemented yet:

    - Decoder Part

    - Leaky ReLUs
    
    - RGB images: Dataloader etc.

    - Never tried anything else than binary segmentation
    
    - if batchsize is chosen to be smaller than the amount of training data, one epoch is equal to one backpropagation for one batch w/ chosen size

    - Different backbone than resnets. A 3D implementation of mobilenetv2 could be promising...

    - The combined dataset cannot be used yet. UKB data has a different depth than meditec data (depth of 81 images). To use UKB and meditec data 
        at the same time, some kind of interpolation for the UKB data needs to be implemented.

    - Probably a lot more...

Result:

    - The best result for the meditec dataset so far was a dice score of 0.685 on the test dataset using resnet18_os8 (os=output
        stride). The model was trained for 700 epochs with all augmentations enabled and then for ~300 epochs with no augmentations
        enabled. The trained model can be found in this repository.
