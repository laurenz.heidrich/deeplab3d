import torch
import numpy as np


def generalized_dice(squared=True, smoothed=False, ignore_bg=True):
    """
    Soft dice loss calculation for arbitrary batch size, number of classes, and number of spatial dimensions.
    Assumes the `channels_first` format.

    # Arguments
        y_true: b x c x X x Y( x Z...) One hot encoding of ground truth
        y_pred: b x c x X x Y( x Z...) Network output, must sum to 1 over c channel (such as after softmax)
        epsilon: Used for numerical stability to avoid divide by zero errors
    """
    def loss(output, label_img, axis=None):
        epsilon = 1e-7

        axes = tuple(range(2, len(output.shape)))
        # skip the batch and class axis for calculating Dice score
        if axis is not None:
            axes = axis

        if ignore_bg:  # ignore background class for computation (last channel)
            label_img = label_img[:, 0:-1, :, :, :]
            output = output[:, 0:-1, :, :, :]

        if squared:  # use squared version
            output = torch.square(output)
            label_img = torch.square(label_img)

        if smoothed:  # use laplacian smoothing
            numerator = 2. * torch.sum(output * label_img, axes) + 1
            denominator = torch.sum(output + label_img, axes) + 1
        else:
            numerator = 2. * torch.sum(output * label_img, axes)
            denominator = torch.sum(output + label_img, axes)

        return torch.mean((numerator + epsilon) / (denominator + epsilon)).detach().numpy()  # average over classes and batch

    return loss

# Copied from nnU-Net
def assert_shape(test, reference):

    assert test.shape == reference.shape, "Shape mismatch: {} and {}".format(
        test.shape, reference.shape)

# Copied from nnU-Net and slightly modified
class ConfusionMatrix:

    def __init__(self, test=None, reference=None):

        self.tp = None
        self.fp = None
        self.tn = None
        self.fn = None
        self.size = None
        self.reference_empty = None
        self.reference_full = None
        self.test_empty = None
        self.test_full = None
        self.set_reference(reference)
        self.set_test(test)

    def set_test(self, test):

        self.test = test
        self.reset()

    def set_reference(self, reference):

        self.reference = reference
        self.reset()

    def reset(self):

        self.tp = None
        self.fp = None
        self.tn = None
        self.fn = None
        self.size = None
        self.test_empty = None
        self.test_full = None
        self.reference_empty = None
        self.reference_full = None

    def compute(self):

        if self.test is None or self.reference is None:
            raise ValueError("'test' and 'reference' must both be set to compute confusion matrix.")

        # We only compute the dice for first class dimension, since in the other dimension TP and TN are switched
        self.test = self.test[:, 0, :, :, :]
        self.reference = self.reference[:, 0, :, :, :]

        assert_shape(self.test, self.reference)

        self.tp = int(((self.test != 0) * (self.reference != 0)).sum())
        self.fp = int(((self.test != 0) * (self.reference == 0)).sum())
        self.tn = int(((self.test == 0) * (self.reference == 0)).sum())
        self.fn = int(((self.test == 0) * (self.reference != 0)).sum())
        self.size = int(np.prod(self.reference.shape, dtype=np.int64))
        self.test_empty = not np.any(self.test)
        self.test_full = np.all(self.test)
        self.reference_empty = not np.any(self.reference)
        self.reference_full = np.all(self.reference)

    def get_matrix(self):

        for entry in (self.tp, self.fp, self.tn, self.fn):
            if entry is None:
                self.compute()
                break

        return self.tp, self.fp, self.tn, self.fn

    def get_size(self):

        if self.size is None:
            self.compute()
        return self.size

    def get_existence(self):

        for case in (self.test_empty, self.test_full, self.reference_empty, self.reference_full):
            if case is None:
                self.compute()
                break

        return self.test_empty, self.test_full, self.reference_empty, self.reference_full


def dice(test=None, reference=None, confusion_matrix=None, nan_for_nonexisting=True, **kwargs):
    """2TP / (2TP + FP + FN)"""

    if confusion_matrix is None:
        confusion_matrix = ConfusionMatrix(test, reference)

    tp, fp, tn, fn = confusion_matrix.get_matrix()
    test_empty, test_full, reference_empty, reference_full = confusion_matrix.get_existence()

    if test_empty and reference_empty:
        if nan_for_nonexisting:
            return float("NaN")
        else:
            return 0.

    return float(2. * tp / (2 * tp + fp + fn))

