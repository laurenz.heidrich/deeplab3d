from network.deeplabv3_3d import DeepLabV3_3D
from data.dataset import Dataset
from pathlib import Path
from utils.utils import add_weight_decay, arguments, write_specs, write_text
from losses import mse_loss, generalized_dice_loss, combined_loss_cce, combined_loss_bce, categorical_crossentropy, \
    binary_crossentropy, categorical_focal_loss, FocalLoss, ce_loss, RobustCrossEntropyLoss, combined_loss_robust_ce
import torch
import numpy as np
import time
import os
import matplotlib.pyplot as plt
from metrics import generalized_dice


def run_training(arg: arguments, arg_val: arguments, output_path=None, loss_id=None, validation=True, resnet="resnet18_os16"):
    #torch.set_default_tensor_type('torch.cuda.FloatTensor')

    path = Path(str(output_path) + "/_" + str(loss_id) + "_" + str(arg.num_epochs) + "_" + str(arg.learning_rate))

    os.mkdir(path)

    fig_path = Path(str(path) + "/epoch_losses_train_" + loss_id + ".png")
    model_path = Path(str(path) + "/model_" + loss_id + ".pth")
    best_model_path = Path(str(path) + "/model_best_" + loss_id + ".pth")
    load_model_path = Path(str(output_path) + "/_ce_nnunet_combined_706_0.0001/model_best_ce_nnunet_combined.pth")
    text_path = Path(str(path) + "/info_" + loss_id + ".txt")
    log_path = Path(str(path) + "/log_" + loss_id + ".txt")
    text = open(text_path, "w")
    text.write(("\n"))
    text.close()
    log = open(text_path, "w")
    log.write(("Start Training\n"))
    log.close()

    args = arg

    write_specs(text_path, args)

    model = DeepLabV3_3D(args, resnet=resnet)
    write_text(log_path, resnet)
    #model.cuda()

    if(args.pretrained):
        print("Using Pretrained Model. Loading Parameters...")
        write_text(log_path, "Using Pretrained Model. Loading Parameters...")
        model.load_state_dict(torch.load(load_model_path))

    dataset = Dataset(args=args)
    train_batch = dataset.batch_generator()

    ########### Validation Data Preperation ###############
    args_val = arg_val
    dataset_val = Dataset(args=args_val)

    val_generator = dataset_val.batch_generator()
    val_batch = next(val_generator)
    val_imgs = torch.tensor(val_batch[0]).float()
    val_label_imgs = torch.tensor(val_batch[1]).float()
    ########### Validation Data Preperation End ###############

    if loss_id == "combined_cce_bg":
        loss_fn = combined_loss_cce(ignore_bg_dice=False)
    elif loss_id == "combined_cce_dice_fg":
        loss_fn = combined_loss_cce(ignore_bg_dice=True)
    elif loss_id == "combined_bce":
        loss_fn = combined_loss_bce()
    elif loss_id == "focal":
        loss_fn = categorical_focal_loss()
        #loss_fn = FocalLoss()
    elif loss_id == "dice":
        loss_fn = generalized_dice_loss()
    elif loss_id == "cce":
        loss_fn = categorical_crossentropy()
    elif loss_id == "ce":
        loss_fn = ce_loss()
    elif loss_id == "ce_nnunet":
        loss_fn = RobustCrossEntropyLoss()
    elif loss_id == "ce_nnunet_combined":
        loss_fn = combined_loss_robust_ce()
    elif loss_id == "bce":
        loss_fn = binary_crossentropy()
    elif loss_id == "mse":
        loss_fn = mse_loss()
    else:
        raise ValueError('Loss Function not known')

    epoch_losses_train = []
    params = model.parameters()
    if args.weight_decay is True:
        params = add_weight_decay(model, l2_value=0.0001)
    optimizer = torch.optim.Adam(params, lr=args.learning_rate)
    model = model.float()
    best_dice = 0
    dices = []

    for epoch in range(args.num_epochs):
        print("###########################")
        print("######## NEW EPOCH ########")
        print("###########################")
        epoch_text = "epoch: %d/%d" % (epoch + 1, args.num_epochs)
        print(epoch_text)

        write_text(log_path, "###########################")
        write_text(log_path, "######## NEW EPOCH ########")
        write_text(log_path, "###########################")
        write_text(log_path, epoch_text)


        ############################################################################
        # train:
        ############################################################################
        model.train()
        current_time = time.time()
        batch = next(train_batch)
        imgs = torch.tensor(batch[0]).float()
        label_imgs = torch.tensor(batch[1]).float()
        outputs = model(imgs)

        # compute the loss:
        loss = loss_fn(outputs, label_imgs)
        loss_value = loss.data.cpu().numpy()

        # optimization step:
        optimizer.zero_grad()  # (reset gradients)
        loss.mean().backward()  # (compute gradients)
        optimizer.step()  # (perform optimization step)

        epoch_loss = np.mean(loss_value)
        epoch_losses_train.append(epoch_loss)

        print("train loss: %g" % epoch_loss)
        write_text(log_path, ("train loss: %g" % epoch_loss))

        ############################################################################
        # validation:
        ############################################################################
        if validation:
            model.eval()
            dice_fn = generalized_dice()

            val_outputs = model(val_imgs)
            dice = dice_fn(val_outputs, val_label_imgs)
            dices.append(dice)

            if dice > best_dice:
                best_dice = dice
                print("New best model! Saving...")
                write_text(log_path, "New best model! Saving...")
                torch.save(model.state_dict(), best_model_path)
                print("Saving done!")
                write_text(log_path, "Saving done!")
                dice_text = "Dice Score on test data: " + str(dice)
                print(dice_text)
                write_text(log_path, dice_text)

            else:
                print("No better dice. Not saving!")
                write_text(log_path, "No better dice. Not saving!")

        plt.figure(1)
        plt.plot(epoch_losses_train, "k^")
        plt.plot(epoch_losses_train, "k")
        if validation:
            plt.plot(dices, "g^")
            plt.plot(dices, "g")
        plt.ylabel("loss/dice")
        plt.xlabel("epoch")
        plt.title("Train Loss and Validation Dice")
        plt.savefig(fig_path)
        plt.close(1)

        print("Epoch Time: ", time.time() - current_time, " seconds")
        print("####")

        write_text(log_path, ("Epoch Time: " + str(time.time() - current_time) + " seconds"))
        write_text(log_path, "####")

    torch.save(model.state_dict(), model_path)


