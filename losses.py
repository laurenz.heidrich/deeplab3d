import torch
import torch.nn as nn
import numpy as np
from torch import Tensor


def ce_loss():

    def loss(output, target):
        # We need target to be a labelmap, not one-hot encoded
        loss_fn = nn.CrossEntropyLoss()
        target_labelmap = torch.argmax(target, dim=1)
        return loss_fn(output, target_labelmap)

    return loss


class RobustCrossEntropyLoss(nn.CrossEntropyLoss):
    """
    this is just a compatibility layer because my target tensor is float and has an extra dimension
    """
    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        if len(target.shape) == len(input.shape):
            target = target[:, 1, :, :, :] #Target is only our foreground class
        return super().forward(input, target.long())



def mse_loss():

    def loss(output, label_img):
        loss_fn = nn.MSELoss()
        return loss_fn(output, label_img)

    return loss


def generalized_dice_loss(squared=True, smoothed=False, ignore_bg=True):
    """
    Soft dice loss calculation for arbitrary batch size, number of classes, and number of spatial dimensions.
    Assumes the `channels_first` format.

    # Arguments
        y_true: b x c x X x Y( x Z...) One hot encoding of ground truth
        y_pred: b x c x X x Y( x Z...) Network output, must sum to 1 over c channel (such as after softmax)
        epsilon: Used for numerical stability to avoid divide by zero errors
    """
    def loss(output, label_img):
        epsilon = 1e-7

        # skip the batch and class axis for calculating Dice score
        axes = tuple(range(2, len(output.shape)))

        if ignore_bg:  # ignore background class for computation (last channel)
            label_img = label_img[:, 0:-1, :, :, :]
            output = output[:, 0:-1, :, :, :]

        if squared:  # use squared version
            output = torch.square(output)
            label_img = torch.square(label_img)

        if smoothed:  # use laplacian smoothing
            numerator = 2. * torch.sum(output * label_img, axes) + 1
            denominator = torch.sum(output + label_img, axes) + 1
        else:
            numerator = 2. * torch.sum(output * label_img, axes)
            denominator = torch.sum(output + label_img, axes)

        return 1 - torch.mean((numerator + epsilon) / (denominator + epsilon))  # average over classes and batch

    return loss


def categorical_crossentropy():

    def loss(output, target, axis=1):
        epsilon = 1e-7
        output = output / torch.sum(output, axis, keepdim=True)
        output = torch.clamp(output, epsilon, 1 - epsilon)
        return -torch.sum(target * torch.log(output), axis)

    return loss


def binary_crossentropy():

    def loss(output, target):
        loss_bce = nn.BCELoss()
        return loss_bce(output, target)

    return loss


def combined_loss_cce(alpha=0.5, ignore_bg_dice=False):

    def loss(output, label_img):
        ce = categorical_crossentropy()
        dl = generalized_dice_loss(ignore_bg=ignore_bg_dice)
        loss_ce = ce(output, label_img)
        loss_dl = dl(output, label_img)

        return torch.sum(((1-alpha) * loss_dl) + (alpha * loss_ce.mean()))

    return loss

def combined_loss_robust_ce(alpha=0.5, ignore_bg_dice=False):

    def loss(output, label_img):
        ce = RobustCrossEntropyLoss()
        dl = generalized_dice_loss(ignore_bg=ignore_bg_dice)
        loss_ce = ce(output, label_img)
        loss_dl = dl(output, label_img)

        return torch.sum(((1-alpha) * loss_dl) + (alpha * loss_ce.mean()))

    return loss


def combined_loss_bce(alpha=0.7):

    def loss(output, label_img):
        ce = binary_crossentropy()
        dl = generalized_dice_loss()
        loss_ce = ce(output, label_img)
        loss_dl = dl(output, label_img)

        return torch.sum(((1-alpha) * loss_dl) + (alpha * loss_ce))

    return loss


def categorical_focal_loss(alpha=.25, gamma=2.):
    """
    Parameters:
      alpha -- the same as weighing factor in balanced cross entropy. Alpha is used to specify the weight of different
      categories/labels, the size of the array needs to be consistent with the number of classes.
      gamma -- focusing parameter for modulating factor (1-p)
    Default value:
      gamma -- 2.0 as mentioned in the paper
      alpha -- 0.25 as mentioned in the paper
    """

    def categorical_focal_loss_fixed(output, target):
        """
        :param y_true: A tensor of the same shape as `y_pred`
        :param y_pred: A tensor resulting from a softmax
        :return: Output tensor.
        """
        output = output.type(torch.FloatTensor)
        target = target.type(torch.FloatTensor)

        # Clip the prediction value to prevent NaN's and Inf's
        epsilon = 1e-7
        output = torch.clamp(output, epsilon, 1 - epsilon)

        # Calculate Cross Entropy
        ce = target * (-torch.log(output))

        # Calculate weight
        ones = torch.ones(size=output.shape, dtype=torch.float32)
        weight = target * torch.pow(torch.sub(ones, output), gamma)

        # Calculate Focal Loss
        loss = alpha * (weight * ce)

        # return mean loss in batch
        return torch.mean(torch.sum(loss, dim=1))

    return categorical_focal_loss_fixed


class FocalLoss(nn.Module):
    """
    copy from: https://github.com/Hsuxu/Loss_ToolBox-PyTorch/blob/master/FocalLoss/FocalLoss.py
    This is a implementation of Focal Loss with smooth label cross entropy supported which is proposed in
    'Focal Loss for Dense Object Detection. (https://arxiv.org/abs/1708.02002)'
        Focal_Loss= -1*alpha*(1-pt)*log(pt)
    :param num_class:
    :param alpha: (tensor) 3D or 4D the scalar factor for this criterion
    :param gamma: (float,double) gamma > 0 reduces the relative loss for well-classified examples (p>0.5) putting more
                    focus on hard misclassified example
    :param smooth: (float,double) smooth value when cross entropy
    :param balance_index: (int) balance class index, should be specific when alpha is float
    :param size_average: (bool, optional) By default, the losses are averaged over each loss element in the batch.
    """

    def __init__(self, alpha=0.25, gamma=2.0, balance_index=0, smooth=1e-5, size_average=True):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.balance_index = balance_index
        self.smooth = smooth
        self.size_average = size_average

        if self.smooth is not None:
            if self.smooth < 0 or self.smooth > 1.0:
                raise ValueError('smooth value should be in [0,1]')

    def forward(self, logit, target):
        num_class = logit.shape[1]
        target = target[:, 0:-1, :, :, :]

        if logit.dim() > 2:
            # N,C,d1,d2 -> N,C,m (m=d1*d2*...)
            logit = logit.view(logit.size(0), logit.size(1), -1)
            logit = logit.permute(0, 2, 1).contiguous()
            logit = logit.view(-1, logit.size(-1))
        target = torch.squeeze(target, 1).contiguous()
        target = target.view(-1, 1)
        print(logit.shape, target.shape)

        alpha = self.alpha

        if alpha is None:
            alpha = torch.ones(num_class, 1)
        elif isinstance(alpha, (list, np.ndarray)):
            assert len(alpha) == num_class
            alpha = torch.FloatTensor(alpha).view(num_class, 1)
            alpha = alpha / alpha.sum()
        elif isinstance(alpha, float):
            alpha = torch.ones(num_class, 1)
            alpha = alpha * (1 - self.alpha)
            alpha[self.balance_index] = self.alpha

        else:
            raise TypeError('Not support alpha type')

        if alpha.device != logit.device:
            alpha = alpha.to(logit.device)

        idx = target.cpu().long()

        one_hot_key = torch.FloatTensor(target.size(0), num_class).zero_()
        one_hot_key = one_hot_key.scatter_(1, idx, 1)
        if one_hot_key.device != logit.device:
            one_hot_key = one_hot_key.to(logit.device)

        if self.smooth:
            one_hot_key = torch.clamp(
                one_hot_key, self.smooth / (num_class - 1), 1.0 - self.smooth)
        pt = (one_hot_key * logit).sum(1)
        pt = pt + self.smooth
        logpt = pt.log()

        gamma = self.gamma

        alpha = alpha[idx]
        alpha = torch.squeeze(alpha)
        loss = -1 * alpha * torch.pow((1 - pt), gamma) * logpt

        if self.size_average:
            loss = loss.mean()
        else:
            loss = loss.sum()
        return loss

